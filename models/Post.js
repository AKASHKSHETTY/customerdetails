const mongoose=require('mongoose');
const postSchema=mongoose.Schema({
    PhoneNumber:{
        type:Number
    },
    CustomerId:{
        type:String,
        
    },
    date:{
        type:Date,
        default:Date.now
    }
});

module.exports=mongoose.model('Posts',postSchema);